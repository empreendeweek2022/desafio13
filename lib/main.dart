import 'package:desafio13/views/home.view.dart';
import 'package:flutter/material.dart';

Future main() async {
  runApp(MyApp());
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {

  @override
  void initState() {
    super.initState();

    initPlatformState();
  }


  Future<void> initPlatformState() async {
    if (!mounted) return;
    // initializations
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Desafio 3 ',
      initialRoute: '/home',
      routes: {
        '/home': (context) => HomeView(),
      },
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      color: Colors.black,
    );
  }
}
